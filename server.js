const express = require('express')
const MongoClient = require('mongodb').MongoClient
const bodyParser = require('body-parser')
const db = require('./config/db')
const app = express()
const port = 8000

app.use(bodyParser.json())
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*')
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE')
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type')
    res.setHeader('Access-Control-Allow-Credentials', true)

    next()
})

MongoClient.connect(db.url, (err, database) => {
  if (err) return console.log(err)

  const db = database.db('db')

  require('./app/routes')(app, db)
  app.listen(port, () => {
    console.log('Server work on port ' + port)
  })               
})