let ObjectID = require('mongodb').ObjectID;

module.exports = function(app, db) {
    app.post('/getData', (req, res) => {
        db.collection('weather').find({}).toArray((err, result) => {
            if (err) {
                res.send({'error':'An error has occurred'})

            } else {
                result.success = true
                res.send(result)
            }
        })
    })

    app.post('/create', (req, res) => {
        let newWeather = req.body

        db.collection('weather').insertOne(newWeather, (err, result) => {
            if (err) { 
                res.send({'success': false, 'error': 'An error has occurred' })

            } else {
                result.ops[0].success = true
                res.send(result.ops[0])
            }
        })
    })

    app.post('/remove', (req, res) => {
        let delId = { '_id': new ObjectID(req.body.id) }

        db.collection('weather').deleteOne(delId, (err, result) => {
            if (err) { 
                res.send({'success': false, 'error': 'An error has occurred' })

            } else {
                res.send({'success': true})
            }
        })
    })
}